<?php

namespace Tests\Feature;

use PHPUnit\Framework\TestCase;
use Speechmatics_PHP\Speechmatics;

class SpeechmaticsGetTest extends TestCase
{
    protected $speechmatics;
    /**
     * @throws \ErrorException
     */
    public function testItReturnsAJsonString()
    {
        $this->getSpeechmatics();

        $response = $this->speechmatics->user();
        $this->assertJson($response);
    }

    public function testItReturnsAValidUserObject()
    {
        $this->getSpeechmatics();

        $response = json_decode($this->speechmatics->user(), true);
        $this->assertArrayHasKey('email', $response['response']['user']);
    }

    public function testItReturnsAListOfJobs()
    {
        $this->getSpeechmatics();

        $response = json_decode($this->speechmatics->jobs(), true);
        $this->assertArrayHasKey('jobs', $response['response']);
    }

    public function testItReturnsASingleJob()
    {
        $this->getSpeechmatics();

        $response = json_decode($this->speechmatics->job(1234), true);
        $this->assertArrayHasKey('id', $response['response']['job']);
    }

    public function testItReturnsATranscription()
    {
        $this->getSpeechmatics();

        $response = json_decode($this->speechmatics->transcript(1234), true);
        $this->assertArrayHasKey('speakers', $response['response']);
    }

    public function testItReturnsAPayment()
    {
        $this->getSpeechmatics();

        $response = json_decode($this->speechmatics->payments(), true);
        $this->assertArrayHasKey('payments', $response['response']);
    }

    /**
     * @return Speechmatics
     * @throws \ErrorException
     */
    private function getSpeechmatics()
    {
        if ( ! $this->speechmatics ) {
            $this->speechmatics = new Speechmatics(
                'ABC1234',
                '12345',
                'https://api.speechmatics.com',
                'v1.0'
            );
        }

        return $this->speechmatics;
    }
}
