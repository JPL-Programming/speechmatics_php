<?php

namespace Speechmatics_PHP;

use Curl\Curl;

class Speechmatics extends Curl
{
    protected $speechmatics;
    protected $api_url;
    protected $api_key;
    protected $api_version;
    protected $user_id;

    public function __construct($api_key, $user_id, $api_url, $api_version)
    {
        $this->api_url = $api_url;
        $this->api_version = $api_version;
        $this->api_key = $api_key;
        $this->user_id = $user_id;

        return $this->speechmatics = parent::__construct($api_url);
    }

    public function user()
    {
        return $this->speechmaticsGet('/' . $this->api_version . '/user/' . $this->user_id . '/?auth_token=' . $this->api_key);
    }

    public function jobs()
    {
        return $this->speechmaticsGet('/' . $this->api_version . '/user/' . $this->user_id . '/jobs/?auth_token=' . $this->api_key);
    }

    public function send($file_location, $model='en', $notification = null, $callback_url = null)
    {
        if ( ! $file_location ) {
            return json_encode([ 'error' => 'Error: File location is required']);
        }

        $this->setHeader('Content-Type', 'multipart/form-data');
        $post_data = [
            'data_file' => new \CurlFile($file_location, 'audio/mpeg', 'audio.mp3'),
            'model' => $model
        ];

        if ( $notification ) {
            $post_data['notification'] = $notification;
        }

        if ( $callback_url && $notification === 'callback') {
            $post_data['callback'] = $callback_url;
        }

        $this->post(
            '/' . $this->api_version . '/user/' . $this->user_id . '/jobs/?auth_token=' . $this->api_key,
            $post_data
        );

        if ( $this->error ) {
            return json_encode([ 'error' => 'Error: ' . $this->errorCode . ': ' . $this->errorMessage ]);
        }

        return json_encode([ 'response' => $this->response ]);
    }

    public function job($job_id)
    {
        return $this->speechmaticsGet('/' . $this->api_version . '/user/' . $this->user_id . '/jobs/' . $job_id . '/?auth_token=' . $this->api_key);
    }

    public function transcript($job_id)
    {
        return $this->speechmaticsGet('/' . $this->api_version . '/user/' . $this->user_id . '/jobs/' . $job_id . '/transcript?auth_token=' . $this->api_key);
    }

    public function alignment($job_id)
    {
        return $this->speechmaticsGet('/' . $this->api_version . '/user/' . $this->user_id . '/jobs/' . $job_id . '/alignment?auth_token=' . $this->api_key);
    }

    public function payments()
    {
        return $this->speechmaticsGet('/' . $this->api_version . '/user/' . $this->user_id . '/payments/?auth_token=' . $this->api_key);
    }

    public function status()
    {
        return $this->speechmaticsGet('/status');
    }

    private function speechmaticsGet($url)
    {
        $this->get($url);

        if ( $this->error ) {
            return json_encode([ 'error' => 'Error: ' . $this->errorCode . ': ' . $this->errorMessage ]);
        }

        return json_encode([ 'response' => $this->response ]);
    }
}